# Gluon build image
This repo periodically builds the [Gluon build Docker image](https://github.com/freifunk-gluon/gluon/blob/master/contrib/docker/Dockerfile) and adds some additional tools on top which are needed for the Freifunk Frankfurt firmware build CI pipeline.
